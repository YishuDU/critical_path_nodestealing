#include "critical_path.hpp"
#include <iostream>
#include "../pempek_assert.hpp"

#include <loguru.hpp>
#include <fstream>

CriticalPath::CriticalPath(Workload *workload,SchedulingDecision *decision, Queue *queue, ResourceSelector *selector,
    double rjms_delay, rapidjson::Document *variant_options, map<string, double> *delay_dict,
    map<string, double> *checkpoint_period_dict, map<string, double> *checkpoint_cost_dict,
    map<string, double> *recovery_cost_dict,
    map<string, double> *Tfirst_dict,
    map<string, double> *Rfirst_dict) :
    ISchedulingAlgorithm(workload, decision, queue, selector, rjms_delay,variant_options)
{
    _delay_dict = delay_dict;
    _checkpoint_period_dict = checkpoint_period_dict;
    _checkpoint_cost_dict = checkpoint_cost_dict;
    _recovery_cost_dict = recovery_cost_dict;
    _Tfirst_dict = Tfirst_dict;
    _Rfirst_dict = Rfirst_dict;
}

CriticalPath::~CriticalPath()
{}

void CriticalPath::on_simulation_start(double date,const rapidjson::Value &batsim_config)
{
    (void) date;
    (void) batsim_config;

    _available_machines.insert(IntervalSet::ClosedInterval(0, _nb_machines - 1));
    _nb_available_machines = _nb_machines;
    PPK_ASSERT_ERROR(_available_machines.size() == (unsigned int) _nb_machines);

    // cerr<<"_nb_machines "<<_nb_machines<<endl;

    // cerr<<"_available_machines_00 "<<_available_machines.to_string_elements()<<endl;
    // cerr<<"_nb_available_machines_00 "<<_nb_available_machines<<endl;
}

void CriticalPath::on_simulation_end(double date)
{
    (void) date;
}


void CriticalPath ::on_machine_available_notify_event(double date, IntervalSet machines)
{
    (void)date;
    _machines_that_became_available_recently += machines;

    for (auto it = _machines_that_became_available_recently.elements_begin();
         it != _machines_that_became_available_recently.elements_end(); ++it)
    {
        // cerr << "_machines_that_became_available_recently_date " << date << endl;
        // cerr << "_machines_that_became_available_recently " << *it << endl;
        _nb_machines++;
        
        if (!_available_machines.contains(*it)){
            _available_machines.insert(*it);
            _nb_available_machines += 1;

        }

        // cerr<<"_available_machines_0 "<<_available_machines.to_string_elements()<<endl;
        // cerr<<"_nb_available_machines_0 "<<_nb_available_machines<<endl;
    }
}

void CriticalPath ::on_machine_unavailable_notify_event(double date, IntervalSet machines)
{
    (void)date;
    _machines_that_became_unavailable_recently += machines;
    
    for (auto it = _machines_that_became_unavailable_recently.elements_begin();
         it != _machines_that_became_unavailable_recently.elements_end(); ++it)
    {
        // cerr << "_machines_that_became_unavailable_recently_date " << date << endl;
        // cerr << "_machines_that_became_unavailable_recently " << *it << endl;
        _nb_machines--;
        
        if (_available_machines.contains(*it)){
            _available_machines.remove(*it);
            _nb_available_machines -= 1;
        }

        // cerr<<"_available_machines_1 "<<_available_machines.to_string_elements()<<endl;
        // cerr<<"_nb_available_machines_1 "<<_nb_available_machines<<endl;
    }
}

void CriticalPath::make_decisions(double date,SortableJobOrder::UpdateInformation *update_info,SortableJobOrder::CompareInformation *compare_info)
{   
    (void) update_info;
    (void) compare_info;

    // cerr<<"date "<<date<<" output the _current_allocations: "<<endl;
    for (unordered_map<string,IntervalSet>::iterator iter = _current_allocations.begin(); iter != _current_allocations.end(); iter++)
    {
        string _current_allocations_job_id = iter->first;
        IntervalSet _current_allocations_processors = iter->second;
        // cerr<<"job_id: "<<_current_allocations_job_id <<" and its processors: " << _current_allocations_processors.to_string_elements()<<endl;
    }


    if (_machines_that_became_unavailable_recently.is_empty() == false)
    {
        for (auto it = _machines_that_became_unavailable_recently.elements_begin();
         it != _machines_that_became_unavailable_recently.elements_end(); ++it)
        {
            // cerr << "_machines_that_became_unavailable_recently_date " << date << endl;
            // cerr << "_machines_that_became_unavailable_recently " << *it << endl;

            for (unordered_map<string,IntervalSet>::iterator iter = _current_allocations.begin(); iter != _current_allocations.end(); iter++)
            {
                string _current_allocations_job_id = iter->first;
                IntervalSet _current_allocations_processors = iter->second;

                if (_current_allocations_processors.contains(*it))
                {
                    // cerr<<"_available_machines_2 "<<_available_machines.to_string_elements()<<endl;
                    // cerr<<"_nb_available_machines_2 "<<_nb_available_machines<<endl;
                    
                    failed_job_id = _current_allocations_job_id;
                    _decision->add_kill_job({failed_job_id}, date);
                    // cerr<<"At date "<<date<<" kill failed job "<<failed_job_id<<endl;

                    _currently_killing = true;

                    //_current_allocations.erase(failed_job_id);

                    // cerr<<"_available_machines_5 "<<_available_machines.to_string_elements()<<endl;
                    // cerr<<"_nb_available_machines_5 "<<_nb_available_machines<<endl;

                    break;
                }
            } 
        }
    }

    // Handle newly finished jobs
    for (const string & ended_job_id : _jobs_ended_recently)
    {
        // cerr << "date "<< date << " _jobs_ended_recently " << ended_job_id << endl;

        if(ended_job_id!=failed_job_id){
            Job * finished_job = (*_workload)[ended_job_id];

            // cerr<<"_current_allocations[ended_job_id] "<<_current_allocations[ended_job_id].to_string_elements()<<endl;
            // cerr<<"finished_job->nb_requested_resources "<<finished_job->nb_requested_resources<<endl;

            // Update data structures
            _available_machines.insert(_current_allocations[ended_job_id]);
            _nb_available_machines += finished_job->nb_requested_resources;
        }   

        _current_allocations.erase(ended_job_id);

        // cerr<<"_available_machines_6 "<<_available_machines.to_string_elements()<<endl;
        // cerr<<"_nb_available_machines_6 "<<_nb_available_machines<<endl;
    }

    // Handle newly released jobs
    for (const string & new_job_id : _jobs_released_recently)
    {   
        // if(new_job_id != failed_job_id_current){
            Job * new_job = (*_workload)[new_job_id];       

            // cerr<<"_jobs_released_recently, its job id "<<new_job_id<< " and its submission_time " << new_job->submission_time<<endl;

            // Is this job valid?
            if (new_job->nb_requested_resources > _nb_machines)
            {
                // Invalid!
                _decision->add_reject_job(new_job_id, date);
                continue;
            }

            // cerr << "job_id_before_append_queue: ";
            // for (auto job_it3 = _queue->begin();job_it3 != _queue->end(); ++job_it3)
            // {
            //     const Job * job_temp3 = (*job_it3)->job;
            //     string job_id_before_append = job_temp3->id;
            //     cerr << job_id_before_append << " "; 
            // }
            // cerr<< endl;  

            //For all newly released jobs, put it in the queue, also include jobs can now be executed 
            _queue->append_job(new_job,update_info);

            // cerr << "job_id_after_append_queue ";
            // for (auto job_it4 = _queue->begin();job_it4 != _queue->end(); ++job_it4)
            // {
            //     const Job * job_temp4 = (*job_it4)->job;
            //     string job_id_after_append = job_temp4->id;
            //     cerr << job_id_after_append << " ";   
            // }
            // cerr<< endl;
        // }
    }
    
    // cerr << "job_id_before_sort_queue ";
    // for (auto job_it1 = _queue->begin();job_it1 != _queue->end(); ++job_it1)
    // {
    //     const Job * job_temp1 = (*job_it1)->job;
    //     string job_id_before = job_temp1->id;
    //     cerr << job_id_before <<" ";   
    // }
    // cerr<< endl;

    if(!_jobs_released_recently.empty() || _currently_killing){
        // cerr<<"date "<< date <<" add_call_me_later "<<endl;
        _decision->add_call_me_later(date+0.1,date);
        _currently_waiting = true;
    }
    
    if (_nopped_recently==true)
    {
        _currently_waiting = false;

        // cerr<<"date "<<date<<" _currently_waiting before "<<_currently_waiting<<endl;
    }

    if(!_currently_waiting)
    {        
        // cerr<<"date "<< date <<" _currently_waiting after "<<_currently_waiting<<endl;

        _currently_killing = false;

        _queue->sort_queue(update_info, compare_info);

        // cerr << "job_id_after_sort_queue ";
        // for (auto job_it2 = _queue->begin();job_it2 != _queue->end(); ++job_it2)
        // {
        //     const Job * job_temp2 = (*job_it2)->job;
        //     string job_id_after = job_temp2->id;
        //     cerr << job_id_after << " ";   
        // }
        // cerr<<endl;

        for (auto job_it = _queue->begin();job_it != _queue->end(); )
        {
            const Job * pending_job = (*job_it)->job;
            IntervalSet machines;
            
            // cerr << "pending_job_id " << pending_job->id << ", and its priority is " << pending_job->priority << endl;

            if (_selector->fit(pending_job, _available_machines, machines))
            {
                // cerr << "_selector fit, pending_job_id " << pending_job->id << endl;
                _decision->add_execute_job(pending_job->id,machines, date);
                _starting_time.insert(make_pair(pending_job->id, date)); //key is the job id, value is the startng time

                string _available_machines_to_string_elements = _available_machines.to_string_elements();
                // cerr<<"_available_machines_to_string_elements "<<_available_machines_to_string_elements<<endl;

                string machines_to_string_elements = machines.to_string_elements();
                // cerr<<"machines_to_string_elements "<<machines_to_string_elements<<endl;               

                // Update data structures
                _available_machines -= machines;
                _nb_available_machines -= pending_job->nb_requested_resources;
                _current_allocations[pending_job->id] = machines;
                job_it = _queue->remove_job(job_it);

                // cerr<<"_available_machines_7 "<<_available_machines.to_string_elements()<<endl;
                // cerr<<"_nb_available_machines_7 "<<_nb_available_machines<<endl;

            }
            else
            {
                // cerr << "_selector not fit, pending_job_id " << pending_job->id << endl;
                // The job becomes priority!
                // As there is no backfilling, we can simply leave this loop.
                break;
            }
        }
    }
}