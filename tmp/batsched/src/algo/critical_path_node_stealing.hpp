#pragma once
using namespace std;

#include <unordered_map>
#include <list>
#include <vector>
#include <set>
#include <map>
#include <array>

#include "../isalgorithm.hpp"
#include "../json_workload.hpp"
#include "../locality.hpp"

class CriticalPathNodeStealing : public ISchedulingAlgorithm
{
public:
    CriticalPathNodeStealing(Workload * workload, SchedulingDecision * decision,
        Queue * queue, ResourceSelector * selector,
        double rjms_delay,
        rapidjson::Document * variant_options, 
        std::map<std::string,double> * delay_dict, 
        std::map<std::string,double> * checkpoint_period_dict, 
        std::map<std::string,double> * checkpoint_cost_dict, 
        std::map<std::string,double> * recovery_cost_dict, 
        std::map<std::string,double> * Tfirst_dict, 
        std::map<std::string,double> * Rfirst_dict);
    virtual ~CriticalPathNodeStealing();

    virtual void on_simulation_start(double date,
        const rapidjson::Value & batsim_config);

    virtual void on_simulation_end(double date);

    virtual void on_machine_available_notify_event(double date, IntervalSet machines);
    virtual void on_machine_unavailable_notify_event(double date, IntervalSet machines);

    virtual void make_decisions(double date,
        SortableJobOrder::UpdateInformation * update_info,
        SortableJobOrder::CompareInformation * compare_info);

private:
    std::map<std::string,double> *_delay_dict;             //key is job_id, value is delay
    std::map<std::string,double> *_checkpoint_period_dict; //key is job_id, value is the checkpoint period
    std::map<std::string,double> *_checkpoint_cost_dict;   //key is job_id, value is the checkpoint cost
    std::map<std::string,double> *_recovery_cost_dict;     //key is job_id, value is the recovery cost
    std::map<std::string,double> *_Tfirst_dict;            //key is job_id, value is the first checkpoint period
    std::map<std::string,double> *_Rfirst_dict;            //key is job_id, value is the first recovery cost

    std::map<std::string,double> _starting_time;  //key is job_id, value is the starting time of each job

    std::map<std::string,int> killed_job_times;   //key is the killed job id, value is how many times it killed
    bool send_profile_if_already_sent = false;   //whether already transmitted profiles should be sent again to Batsim or not
    bool send_profiles_in_separate_event = true; //whether profiles should be sent in a separate message or not
    std::set<std::string> profiles_already_sent;

    // Machines currently available
    IntervalSet _available_machines;
    int _nb_available_machines = -1;
    bool _currently_waiting = false;

    bool _currently_killing = false;

    // string failed_job_id_current=NULL;
    
    // Allocations of running jobs
    std::unordered_map<std::string, IntervalSet> _current_allocations;

    string failed_job_id;
};
