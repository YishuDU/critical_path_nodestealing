#include "critical_path_node_stealing.hpp"
#include <iostream>
#include "../pempek_assert.hpp"

#include <loguru.hpp>
#include <fstream>

CriticalPathNodeStealing::CriticalPathNodeStealing(Workload *workload,SchedulingDecision *decision, Queue *queue, ResourceSelector *selector,
    double rjms_delay, rapidjson::Document *variant_options, map<string, double> *delay_dict,
    map<string, double> *checkpoint_period_dict, map<string, double> *checkpoint_cost_dict,
    map<string, double> *recovery_cost_dict,
    map<string, double> *Tfirst_dict,
    map<string, double> *Rfirst_dict) :
    ISchedulingAlgorithm(workload, decision, queue, selector, rjms_delay, variant_options)
{
    _delay_dict = delay_dict;
    _checkpoint_period_dict = checkpoint_period_dict;
    _checkpoint_cost_dict = checkpoint_cost_dict;
    _recovery_cost_dict = recovery_cost_dict;
    _Tfirst_dict = Tfirst_dict;
    _Rfirst_dict = Rfirst_dict;
}

CriticalPathNodeStealing::~CriticalPathNodeStealing()
{}

void CriticalPathNodeStealing::on_simulation_start(double date,const rapidjson::Value &batsim_config)
{
    (void) date;
    (void) batsim_config;

    _available_machines.insert(IntervalSet::ClosedInterval(0, _nb_machines - 1));
    _nb_available_machines = _nb_machines;
    PPK_ASSERT_ERROR(_available_machines.size() == (unsigned int) _nb_machines);

    // cerr<<"_nb_machines "<<_nb_machines<<endl;
}

void CriticalPathNodeStealing::on_simulation_end(double date)
{
    (void) date;
}


void CriticalPathNodeStealing ::on_machine_available_notify_event(double date, IntervalSet machines)
{
    (void)date;
    _machines_that_became_available_recently += machines;

    for (auto it = _machines_that_became_available_recently.elements_begin();
         it != _machines_that_became_available_recently.elements_end(); ++it)
    {
        // cerr << "_machines_that_became_available_recently_date " << date << endl;
        // cerr << "_machines_that_became_available_recently " << *it << endl;
        _nb_machines++;

        if (!_available_machines.contains(*it)){
            _available_machines.insert(*it);
            _nb_available_machines += 1;

        }

        // cerr<<"_available_machines_0 "<<_available_machines.to_string_elements()<<endl;
        // cerr<<"_nb_available_machines_0 "<<_nb_available_machines<<endl;
    }
}

void CriticalPathNodeStealing ::on_machine_unavailable_notify_event(double date, IntervalSet machines)
{
    (void)date;
    _machines_that_became_unavailable_recently += machines;
    
    for (auto it = _machines_that_became_unavailable_recently.elements_begin();
         it != _machines_that_became_unavailable_recently.elements_end(); ++it)
    {
        // cerr << "_machines_that_became_unavailable_recently_date " << date << endl;
        // cerr << "_machines_that_became_unavailable_recently " << *it << endl;
        _nb_machines--;

        if (_available_machines.contains(*it)){
            _available_machines.remove(*it);
            _nb_available_machines -= 1;
        }

        // cerr<<"_available_machines_1 "<<_available_machines.to_string_elements()<<endl;
        // cerr<<"_nb_available_machines_1 "<<_nb_available_machines<<endl;
    }
}

void CriticalPathNodeStealing::make_decisions(double date,SortableJobOrder::UpdateInformation *update_info,SortableJobOrder::CompareInformation *compare_info)
{
    // cerr<<"date "<<date<<" output the _current_allocations: "<<endl;
    for (unordered_map<string,IntervalSet>::iterator iter = _current_allocations.begin(); iter != _current_allocations.end(); iter++)
    {
        string _current_allocations_job_id = iter->first;
        IntervalSet _current_allocations_processors = iter->second;
        // cerr<<"job_id: "<<_current_allocations_job_id <<" and its processors: " << _current_allocations_processors.to_string_elements()<<endl;
    }

    if (_machines_that_became_unavailable_recently.is_empty() == false)
    {
        for (auto it = _machines_that_became_unavailable_recently.elements_begin();
         it != _machines_that_became_unavailable_recently.elements_end(); ++it)
        {
            // cerr << "_machines_that_became_unavailable_recently_date " << date << endl;
            // cerr << "_machines_that_became_unavailable_recently " << *it << endl;

            for (unordered_map<string,IntervalSet>::iterator iter = _current_allocations.begin(); iter != _current_allocations.end(); iter++)
            {
                string _current_allocations_job_id = iter->first;
                IntervalSet _current_allocations_processors = iter->second;

                if (_current_allocations_processors.contains(*it))
                {
                    // cerr<<"_available_machines_2 "<<_available_machines.to_string_elements()<<endl;
                    // cerr<<"_nb_available_machines_2 "<<_nb_available_machines<<endl;
                    
                    failed_job_id = _current_allocations_job_id;
                    _decision->add_kill_job({failed_job_id}, date);
                    // cerr<<"At date "<<date<<" kill failed job "<<failed_job_id<<endl;

                    _currently_killing = true;

                    //compute the bottom level (priority) of the remainging part of the failed job
                    Job *job_failed = (*_workload)[failed_job_id];

                    // failed_job_id_current = failed_job_id;
                    // cerr<<"failed_job_id_current "<<failed_job_id_current<<endl;
                    // _queue->append_job(job_failed,update_info);

                    // cerr << "job_id_queue_after_kill_failed_job ";
                    // for (auto job_it1 = _queue->begin();job_it1 != _queue->end(); ++job_it1)
                    // {
                    //     const Job * job_temp1 = (*job_it1)->job;
                    //     string job_id = job_temp1->id;
                    //     cerr << job_id <<" ";   
                    // }
                    // cerr<< endl;


                    map<string, double>::iterator iter_ckp_period_f = _checkpoint_period_dict->find(failed_job_id);
                    double T_f = iter_ckp_period_f->second;

                    map<string, double>::iterator iter_ckp_cost_f = _checkpoint_cost_dict->find(failed_job_id);
                    double C_f = iter_ckp_cost_f->second;

                    map<string, double>::iterator iter_Tfirst_f = _Tfirst_dict->find(failed_job_id);
                    double Tfirst_f = iter_Tfirst_f->second;

                    map<string, double>::iterator iter_Rfirst_f = _Rfirst_dict->find(failed_job_id);
                    double Rfirst_f = iter_Rfirst_f->second;

                    map<string, double>::iterator iter_starting_time_f = _starting_time.find(failed_job_id);
                    double t_start_f = iter_starting_time_f->second;

                    // cerr<<"In critical_path_node_stealing.cpp, T_f "<<T_f<<endl;
                    // cerr<<"In critical_path_node_stealing.cpp, C_f "<<C_f<<endl;
                    // cerr<<"In critical_path_node_stealing.cpp, Tfirst_f "<<Tfirst_f<<endl;
                    // cerr<<"In critical_path_node_stealing.cpp, Rfirst_f "<<Rfirst_f<<endl;
                    // cerr<<"In critical_path_node_stealing.cpp, t_start_f "<<t_start_f<<endl;

                    double failed_t1 = Rfirst_f+Tfirst_f+C_f;
                    double failed_resubmission_priority;

                    if(date <= t_start_f+failed_t1){
                        failed_resubmission_priority = job_failed->priority;
                    }
                    else{
                        failed_resubmission_priority = job_failed->priority - (Tfirst_f+C_f+floor((date-t_start_f-failed_t1)/(T_f+C_f))*(T_f+C_f));
                    }

                    // cerr<<"the priority of the failed job "<<job_failed->priority<<endl;
                    // cerr<<"the priority of the resubmission of the failed job  "<<failed_resubmission_priority<<endl;

                    // cerr<<"When a failure occurs at date "<<date<<" the current allocations is as following "<<endl;
                    // for (unordered_map<string,IntervalSet>::iterator iter0 = _current_allocations.begin(); iter0 != _current_allocations.end(); iter0++)
                    // {
                    //     cerr<<"job_id: "<<iter0->first <<" and its processors: " << (iter0->second).to_string_elements()<<endl;
                    // }

                    // Update data structures because the failed job did not release the free processor, thus we did not add these failed processors
                    // _available_machines.insert(_current_allocations[ended_job_id]);
                    // _nb_available_machines += finished_job->nb_requested_resources;
                   
                   // _current_allocations.erase(failed_job_id);

                    
                    // cerr<<"_available_machines_3 "<<_available_machines.to_string_elements()<<endl;
                    // cerr<<"_nb_available_machines_3 "<<_nb_available_machines<<endl;

                    if (_nb_available_machines == 0){
                        //find the victim job whose remaining part's priority is minimal
                        double victim_resubmission_priority = 1e9;
                        double victim_resubmission_priority_temp;
                        Job * job_victim = NULL;
                        Job * job_temp = NULL;
                        double T_v;
                        double C_v;
                        double Tfirst_v;
                        double Rfirst_v;
                        double t_start_v;
                        double victim_t1;
                        string job_temp_id;

                        // cerr<<"When a failure occurs at date "<<date<<endl;
                        for (unordered_map<string,IntervalSet>::iterator iter1 = _current_allocations.begin(); iter1 != _current_allocations.end(); iter1++)
                        {
                            // cerr<<"job_id: "<<iter1->first <<" and its processors: " << (iter1->second).to_string_elements()<<endl;
                            job_temp = (*_workload)[iter1->first];
                            job_temp_id = job_temp->id;

                            //compute the bottom level (priority) of the remainging part of the victim job
                            map<string, double>::iterator iter_ckp_period_v = _checkpoint_period_dict->find(job_temp_id);
                            T_v = iter_ckp_period_v->second;

                            map<string, double>::iterator iter_ckp_cost_v = _checkpoint_cost_dict->find(job_temp_id);
                            C_v = iter_ckp_cost_v->second;

                            map<string, double>::iterator iter_Tfirst_v = _Tfirst_dict->find(job_temp_id);
                            Tfirst_v = iter_Tfirst_v->second;

                            map<string, double>::iterator iter_Rfirst_v = _Rfirst_dict->find(job_temp_id);
                            Rfirst_v = iter_Rfirst_v->second;

                            map<string, double>::iterator iter_starting_time_v = _starting_time.find(job_temp_id);
                            t_start_v = iter_starting_time_v->second;

                            // cerr<<"In critical_path_node_stealing.cpp, T_v "<<T_v<<endl;
                            // cerr<<"In critical_path_node_stealing.cpp, C_v "<<C_v<<endl;
                            // cerr<<"In critical_path_node_stealing.cpp, Tfirst_v "<<Tfirst_v<<endl;
                            // cerr<<"In critical_path_node_stealing.cpp, Rfirst_v "<<Rfirst_v<<endl;
                            // cerr<<"In critical_path_node_stealing.cpp, t_start_v "<<t_start_v<<endl;

                            victim_t1 = Rfirst_v+Tfirst_v+C_v;
                            if(date <= t_start_v+victim_t1){
                                victim_resubmission_priority_temp = job_temp->priority;
                            }
                            else{
                                victim_resubmission_priority_temp = job_temp->priority - (Tfirst_v+C_v+floor((date-t_start_v-victim_t1)/(T_v+C_v))*(T_v+C_v));
                            }
                            

                            // cerr<<"victim_priority_temp"<<job_temp->priority<<endl;
                            // cerr<<"victim_resubmission_priority_temp "<<victim_resubmission_priority_temp<<endl;

                            if(victim_resubmission_priority_temp < victim_resubmission_priority){
                                // cerr<<"victim_resubmission_priority "<<victim_resubmission_priority<<endl;
                                // cerr<<"priority_min "<<priority_min<<endl;
                                victim_resubmission_priority = victim_resubmission_priority_temp;
                                job_victim = job_temp;
                            }
                        }
                        string victim_job_id = job_victim->id;
                        // cerr<<"victim_job_id "<<victim_job_id<<endl;
                        // cerr<<"victim_resubmission_priority "<<victim_resubmission_priority<<endl;

                        //Find the job with the smallest priority in the queue
                        // double priority_queue_min = 1e9;
                        // double priority_queue_temp;
                        // const Job * job_temp_queue = NULL;
                        // string job_id_queue;
                        // cerr << "job_id_queue ";
                        // for (auto job_it_queue = _queue->begin();job_it_queue != _queue->end(); ++job_it_queue)
                        // {
                        //     job_temp_queue = (*job_it_queue)->job;
                        //     priority_queue_temp = job_temp_queue->priority;

                        //     job_id_queue = job_temp_queue->id;
                        //     cerr << job_id_queue << " " << priority_queue_temp << " ";   

                        //     if(priority_queue_temp<priority_queue_min){
                        //         priority_queue_min = priority_queue_temp;
                        //     }
                        // }
                        // cerr<<endl;
                        // cerr<<"priority_queue_min "<<priority_queue_min<<endl;

                        //Find the job with the largest priority in the queue
                        double priority_queue_max = 0;
                        double priority_queue_temp;
                        const Job * job_temp_queue = NULL;
                        string job_id_queue;
                        // cerr << "job_id_queue ";
                        for (auto job_it_queue = _queue->begin();job_it_queue != _queue->end(); ++job_it_queue)
                        {
                            job_temp_queue = (*job_it_queue)->job;
                            priority_queue_temp = job_temp_queue->priority;

                            job_id_queue = job_temp_queue->id;
                            // cerr << job_id_queue << " " << priority_queue_temp << " ";   

                            if(priority_queue_temp > priority_queue_max){
                                priority_queue_max = priority_queue_temp;
                            }
                        }
                        // cerr<<endl;
                        // cerr<<"priority_queue_max "<<priority_queue_max<<endl;


                        //kill victim job when the following two conditions are both statisfied
                        //There are three variants: 1+2, 1+3, 1+4

                        //1. when the bottom level of the remaining part of the failed job is larger than 
                        //the bottom level of the remaining part of the victim job

                        //2. when the bottom level of the remaining part of the failed job is larger than
                        //the maximum bottom level of all jobs in the queue

                        //3. set the priority of the remaining part of the failed job is largest so that it can always
                        //be executed immediately

                        //4. when the bottom level of the remaining part of the failed job is smaller than
                        //the minimal bottom level of all jobs in the queue (test this variant in the future)
                        
                        // cerr<<"failed_resubmission_priority "<<failed_resubmission_priority<<endl;
                        // cerr<<"victim_resubmission_priority "<<victim_resubmission_priority<<endl;
                        // cerr<<"priority_queue_max "<<priority_queue_max<<endl;
                        
                        //Node Stealing Version I
                        //variant 1 + 2 
                        if((failed_resubmission_priority > victim_resubmission_priority) && (failed_resubmission_priority > priority_queue_max))
                        {
                            _decision->add_kill_job({victim_job_id}, date);
                            cerr<<"At date "<< date <<" kill victim job "<<victim_job_id<<endl;

                            // _available_machines.insert(_current_allocations[victim_job_id]);
                            // _nb_available_machines += job_victim->nb_requested_resources;

                            // _current_allocations.erase(victim_job_id);
                        }

                        //Node Stealing Version II
                        //variant 1 + 3
                        // if((failed_resubmission_priority > victim_resubmission_priority))
                        // {
                        //     _decision->add_kill_job({victim_job_id}, date);
                        //     // cerr<<"At date "<< date <<" kill victim job "<<victim_job_id<<endl;
                        //     //_current_allocations.erase(victim_job_id);

                        //     // _available_machines.insert(_current_allocations[victim_job_id]);
                        //     // _nb_available_machines += job_victim->nb_requested_resources;
                        //     // cerr<<"_available_machines_4 "<<_available_machines.to_string_elements()<<endl;
                        //     // cerr<<"_nb_available_machines_4 "<<_nb_available_machines<<endl;
                        // }
                        
                        //variant 1 + 4
                        //if(failed_resubmission_priority > victim_resubmission_priority && failed_resubmission_priority < priority_queue_min)
                        // {
                        //     _decision->add_kill_job({victim_job_id}, date);
                        //     cerr<<"kill victim job "<<victim_job_id<<endl;

                        //     _available_machines.insert(_current_allocations[victim_job_id]);
                        //     _nb_available_machines += job_victim->nb_requested_resources;
                        //     _current_allocations.erase(victim_job_id);
                        // }

                        // cerr<<"_available_machines_5 "<<_available_machines.to_string_elements()<<endl;
                        // cerr<<"_nb_available_machines_5 "<<_nb_available_machines<<endl;

                        break;
                    } 
                }
            } 
        }
    }

    (void) update_info;
    (void) compare_info;

    // Handle newly finished jobs
    for (const string & ended_job_id : _jobs_ended_recently)
    {
        // cerr << "_jobs_ended_recently " << ended_job_id << endl;

        //only when ended job id is not failed job id, we update date structures
        //but when ended job id is failed job id, we still need to erase it from _current_allocations
        if(ended_job_id!=failed_job_id){
            Job * finished_job = (*_workload)[ended_job_id];

            // cerr<<"_current_allocations[ended_job_id] "<<_current_allocations[ended_job_id].to_string_elements()<<endl;
            // cerr<<"finished_job->nb_requested_resources "<<finished_job->nb_requested_resources<<endl;

            // Update data structures
            _available_machines.insert(_current_allocations[ended_job_id]);
            _nb_available_machines += finished_job->nb_requested_resources;
            
        } 

        _current_allocations.erase(ended_job_id);

        // cerr<<"_available_machines_6 "<<_available_machines.to_string_elements()<<endl;
        // cerr<<"_nb_available_machines_6 "<<_nb_available_machines<<endl;       
    }

    // Handle newly released jobs
    for (const string & new_job_id : _jobs_released_recently)
    {   
        // if(new_job_id != failed_job_id_current){
            Job * new_job = (*_workload)[new_job_id];       

            // cerr<<"_jobs_released_recently, its job id "<<new_job_id<< " and its submission_time " << new_job->submission_time<<endl;

            // Is this job valid?
            if (new_job->nb_requested_resources > _nb_machines)
            {
                // Invalid!
                _decision->add_reject_job(new_job_id, date);
                continue;
            }

            // cerr << "job_id_before_append_queue: ";
            // for (auto job_it3 = _queue->begin();job_it3 != _queue->end(); ++job_it3)
            // {
            //     const Job * job_temp3 = (*job_it3)->job;
            //     string job_id_before_append = job_temp3->id;
            //     cerr << job_id_before_append << " "; 
            // }
            // cerr<< endl;  

            //For all newly released jobs, put it in the queue, also include can now be executed jobs
            _queue->append_job(new_job,update_info);

            // cerr << "job_id_after_append_queue ";
            // for (auto job_it4 = _queue->begin();job_it4 != _queue->end(); ++job_it4)
            // {
            //     const Job * job_temp4 = (*job_it4)->job;
            //     string job_id_after_append = job_temp4->id;
            //     cerr << job_id_after_append << " ";   
            // }
            // cerr<< endl;
        // }
    }
    
    // cerr << "job_id_before_sort_queue ";
    // for (auto job_it1 = _queue->begin();job_it1 != _queue->end(); ++job_it1)
    // {
    //     const Job * job_temp1 = (*job_it1)->job;
    //     string job_id_before = job_temp1->id;
    //     cerr << job_id_before <<" ";   
    // }
    // cerr<< endl;

    if(!_jobs_released_recently.empty() || _currently_killing){
        // cerr<<"date "<< date <<" add_call_me_later "<<endl;
        _decision->add_call_me_later(date+0.1,date);
        _currently_waiting = true;
    }
    
    if (_nopped_recently==true)
    {
        _currently_waiting = false;

        // cerr<<"date "<<date<<" _currently_waiting before "<<_currently_waiting<<endl;
    }

    if(!_currently_waiting)
    {        
        // cerr<<"date "<< date <<" _currently_waiting after "<<_currently_waiting<<endl;

        _currently_killing = false;

        _queue->sort_queue(update_info, compare_info);

        // cerr << "job_id_after_sort_queue ";
        // for (auto job_it2 = _queue->begin();job_it2 != _queue->end(); ++job_it2)
        // {
        //     const Job * job_temp2 = (*job_it2)->job;
        //     string job_id_after = job_temp2->id;
        //     cerr << job_id_after << " ";   
        // }
        // cerr<<endl;

        for (auto job_it = _queue->begin();job_it != _queue->end(); )
        {
            const Job * pending_job = (*job_it)->job;
            IntervalSet machines;
            
            // cerr << "pending_job_id " << pending_job->id << ", and its priority is " << pending_job->priority << endl;

            if (_selector->fit(pending_job, _available_machines, machines))
            {
                // cerr << "_selector fit, pending_job_id " << pending_job->id << endl;
                _decision->add_execute_job(pending_job->id,machines, date);
                _starting_time.insert(make_pair(pending_job->id, date)); //key is the job id, value is the startng time

                string _available_machines_to_string_elements = _available_machines.to_string_elements();
                // cerr<<"_available_machines_to_string_elements "<<_available_machines_to_string_elements<<endl;

                string machines_to_string_elements = machines.to_string_elements();
                // cerr<<"machines_to_string_elements "<<machines_to_string_elements<<endl;               

                // Update data structures
                _available_machines -= machines;
                _nb_available_machines -= pending_job->nb_requested_resources;
                _current_allocations[pending_job->id] = machines;
                job_it = _queue->remove_job(job_it);

                // cerr<<"_available_machines_7 "<<_available_machines.to_string_elements()<<endl;
                // cerr<<"_nb_available_machines_7 "<<_nb_available_machines<<endl;
            }
            else
            {
                // cerr << "_selector not fit, pending_job_id " << pending_job->id << endl;
                // The job becomes priority!
                // As there is no backfilling, we can simply leave this loop.
                break;
            }
        }
    }
}