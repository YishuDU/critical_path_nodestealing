#!/usr/bin/bash

### generate yaml files 

for e in 0
do
    mkdir -p test_criticalpath_${e}
    cd test_criticalpath_${e}

    robin generate "test_criticalpath_${e}.yaml" \
    --output-dir "log_criticalpath_${e}" \
    --batcmd "batsim -p ../platforms/cluster100.xml -W ../workloads/cholesky_16.dax -e log_criticalpath_${e}/expe_criticalpath_${e} --forward-profiles-on-submission --events ../events/events_Cholesky_${e}.txt" \
    --schedcmd "batsched -v critical_path --queue_order fcfs_prio --call_make_decisions_on_single_nop=1" 

    cd ..
done


for e in 0
do
    mkdir -p test_criticalpathnodestealing_${e}
    cd test_criticalpathnodestealing_${e}

    robin generate "test_criticalpathnodestealing_${e}.yaml" \
    --output-dir "log_criticalpathnodestealing_${e}" \
    --batcmd "batsim -p ../platforms/cluster100.xml -W ../workloads/cholesky_16.dax -e log_criticalpathnodestealing_${e}/expe_criticalpathnodestealing_${e} --forward-profiles-on-submission --events ../events/events_Cholesky_${e}.txt" \
    --schedcmd "batsched -v critical_path_node_stealing --queue_order fcfs_prio --call_make_decisions_on_single_nop=1" 

    cd ..
done



