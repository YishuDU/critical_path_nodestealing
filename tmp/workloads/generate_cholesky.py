#!/opt/local/bin/python
###!/opt/local/Library/Frameworks/Python.framework/Versions/2.7/Resources/Python.app/Contents/MacOS/Python
import os
import networkx as nx
import heapq
import math
import sys
# from PIL import Image
# import logging
# import ImageDraw
# import ImageFont


timings = dict()
# timings['cpu'] = {'POTRF': 450.0, 'GEMM':1450.0, 'SYRK':990.0, 'TRSM':830.0}
# timings['gpu'] = {'POTRF': 10490.0, 'GEMM':140.0, 'SYRK':150.0, 'TRSM':400.0}
# timings['cpu'] = {'POTRF': 100.0, 'GEMM':431.0, 'SYRK':431.0, 'TRSM':234.0} #This is the original cpu time
# timings['gpu'] = {'POTRF': 44.0, 'GEMM':3.0, 'SYRK':3.0, 'TRSM':7.0}

timings['cpu'] = {'POTRF': 100.0, 'GEMM':441.0, 'SYRK':441.0, 'TRSM':239.0} #This is the cpu time including checkpoint time, checkpoint period is 173.2
timings['gpu'] = {'POTRF': 44.0, 'GEMM':3.0, 'SYRK':3.0, 'TRSM':7.0}



def generate_cholesky(N):
    graph = nx.DiGraph()
    max_index_len = int(math.ceil(math.log(N)/math.log(10)))
    int_format = "%0"+str(max_index_len)+"d"
    # We first generate all nodes
    for k in range(N):
        k_str = int_format % k
        graph.add_node('POTRF_'+k_str, type='POTRF', color='red', style='filled') # tile k,k
        for m in range(k+1,N):
            m_str = int_format % m
            graph.add_node('TRSM_'+m_str+'_'+k_str, type='TRSM', color='lightblue', style='filled') # tile m,k
        for n in range(k+1,N):
            n_str = int_format % n
            graph.add_node('SYRK_'+n_str+'_'+k_str, type='SYRK', color='yellow', style='filled') # tile n,n
            #graph.add_node('GEMM_'+n_str+'_'+n_str+'_'+k_str, type='GEMM', color='yellow', style='filled') # tile n,n
            for m in range(n+1,N):
                m_str = int_format % m                
                graph.add_node('GEMM_'+m_str+'_'+n_str+'_'+k_str, type='GEMM', color='green', style='filled') # tile m,n


    # Then, we generate all dependencies (inputs)
    for k in range(N):
        k_str = int_format % k
        k_minus_one_str = int_format % (k-1)
        if k>0:
            graph.add_edge('SYRK_'+k_str+'_'+k_minus_one_str, 'POTRF_'+k_str)
            #graph.add_edge('GEMM_'+k_str+'_'+k_str+'_'+k_minus_one_str, 'POTRF_'+k_str)
        for m in range(k+1,N):
            m_str = int_format % m
            graph.add_edge('POTRF_'+k_str, 'TRSM_'+m_str+'_'+k_str)
            if k>0:
                graph.add_edge('GEMM_'+m_str+'_'+k_str+'_'+k_minus_one_str, 'TRSM_'+m_str+'_'+k_str)
        for n in range(k+1,N):
            n_str = int_format % n
            graph.add_edge('TRSM_'+n_str+'_'+k_str, 'SYRK_'+n_str+'_'+k_str)
            # graph.add_edge('TRSM_'+n_str+'_'+k_str, 'GEMM_'+n_str+'_'+n_str+'_'+k_str)
            if k>0:
                graph.add_edge('SYRK_'+n_str+'_'+k_minus_one_str, 'SYRK_'+n_str+'_'+k_str)
                # graph.add_edge('GEMM_'+n_str+'_'+n_str+'_'+k_minus_one_str, 'GEMM_'+n_str+'_'+n_str+'_'+k_str)
            for m in range(n+1,N):
                m_str = int_format % m
                graph.add_edge('TRSM_'+n_str+'_'+k_str, 'GEMM_'+m_str+'_'+n_str+'_'+k_str)
                graph.add_edge('TRSM_'+m_str+'_'+k_str, 'GEMM_'+m_str+'_'+n_str+'_'+k_str)
                if k>0:
                    graph.add_edge('GEMM_'+m_str+'_'+n_str+'_'+k_minus_one_str, 'GEMM_'+m_str+'_'+n_str+'_'+k_str)

    # if N<=12:
    #     nx.write_dot(graph,"cholesky_"+str(N)+".dot")
    #     os.system("dot -Tpdf -ocholesky_"+str(N)+".pdf cholesky_"+str(N)+".dot")
    #     nx.write_graphml(graph, "cholesky.graphml")

    filename="cholesky_"+str(N)+".dax"
    f = open(filename, 'w', encoding="utf-8")
    
    N=len(graph.nodes())
    M=len(graph.edges())
    f.write(f"<adag xmlns=\"http://pegasus.isi.edu/schema/DAX\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/DAX http://pegasus.isi.edu/schema/dax-2.1.xsd\" version=\"2.1\" count=\"1\" index=\"0\" name=\"test\" jobCount=\"{N}\" fileCount=\"0\" childCount=\"{M}\">\n")

    #by myself, add 2000 independet jobs, the length of each job is 30*60, thus the delay is 30*60 + floor(30*60/173.2)*5 = 1850
    for i in range(0, 2000):
        temp = '  <job id=\"INDE_%d\" runtime=\"1850\" name=\"INDE_%d\"></job> ' % (i, i)
        x = str(temp) + '\n'
        f.write(x)


    for node in graph.nodes():
        time=timings['cpu'][graph.nodes.data()[node]['type']]
        f.write(f"  <job id=\"{node}\" runtime=\"{time}\" name=\"{node}\"></job>\n")

    f.write("\n")
    for edge in graph.edges():
        src=edge[0]
        dst=edge[1]
        f.write(f"  <child ref=\"{dst}\"><parent ref=\"{src}\"/></child>\n") #add </child> by myself to meet the batsim requirement
    f.write("</adag>\n")
    f.close
    print(f"Generated file \"{filename}\" with {N} tasks and {M} dependencies.")

    return graph


if (len(sys.argv) != 2 or not sys.argv[1].isnumeric()):
    print("Error: provide the dimension of the matrix (in tiles) as a parameter")
    print(f"  Usage: {sys.argv[0]} <int>")
    exit(1)
    
generate_cholesky(int(sys.argv[1]))
