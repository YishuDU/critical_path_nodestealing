#!/usr/bin/bash

## simulation for toy example

# printf "The simulation is running\n"
# for e in 0
# do
#     printf "The failure scenario %d is running\n" $e

    # # Fault free execution of toy example
    # cd test_0_0_smallexample
    # robin ./test_0_0_smallexample.yaml
    # cd ..

    # # Critical Path scheduling without node stealing of toy example
    # cd test_0_1_smallexample
    # robin ./test_0_1_smallexample.yaml
    # cd ..

    # # Critical Path scheduling with node stealing of toy example
    # cd test_0_2_smallexample
    # robin ./test_0_2_smallexample.yaml
    # cd ..
# done 


# for e in 0
# do
#     printf "The failure scenario %d is running\n" $e

#     # Faultfree of Tiled Cholesky Factorization
#     cd test_Cholesky16_faultfree
#     robin ./test_Cholesky16_faultfree.yaml
#     cd ..
# done 


printf "The simulation is running\n"
for e in 0
do
    printf "The failure scenario %d is running\n" $e

    # Critical Path scheduling without node stealing of Tiled Cholesky Factorization
    cd test_criticalpath_${e}
    robin ./test_criticalpath_${e}.yaml
    cd ..
done 


printf "The simulation is running\n"
for e in 0
do
    printf "The failure scenario %d is running\n" $e

    # Critical Path scheduling with node stealing of Tiled Cholesky Factorization
    cd test_criticalpathnodestealing_${e}
    robin ./test_criticalpathnodestealing_${e}.yaml
    cd ..
done 