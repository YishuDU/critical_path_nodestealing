#!/usr/bin/bash

### catalog h0 and h111 simulation results for June 2017

mkdir -p test_smallexample

for e in 0
do
    cp ./test_${e}_0_smallexample/COMPLETED_KILLED.txt ./test_smallexample/COMPLETED_KILLED_${e}_0.txt
    cp ./test_${e}_0_smallexample/COMPLETED_SUCCESSFULLY_JOBCOPY.txt ./test_smallexample/COMPLETED_SUCCESSFULLY_JOBCOPY_${e}_0.txt
    cp ./test_${e}_0_smallexample/node_stealing_info.txt ./test_smallexample/node_stealing_info_${e}_0.txt
    cp ./test_${e}_0_smallexample/log_${e}_0/expe_${e}_0_jobs.csv ./test_smallexample/expe_${e}_0_jobs.csv
    cp ./test_${e}_0_smallexample/log_${e}_0/expe_${e}_0_machine_states.csv ./test_smallexample/expe_${e}_0_machine_states.csv
    cp ./test_${e}_0_smallexample/log_${e}_0/expe_${e}_0_schedule.csv ./test_smallexample/expe_${e}_0_schedule.csv


    cp ./test_${e}_111_smallexample/COMPLETED_KILLED.txt ./test_smallexample/COMPLETED_KILLED_${e}_111.txt
    cp ./test_${e}_111_smallexample/COMPLETED_SUCCESSFULLY_JOBCOPY.txt ./test_smallexample/COMPLETED_SUCCESSFULLY_JOBCOPY_${e}_111.txt
    cp ./test_${e}_111_smallexample/node_stealing_info.txt ./test_smallexample/node_stealing_info_${e}_111.txt
    cp ./test_${e}_111_smallexample/node_stealing_times.txt ./test_smallexample/node_stealing_times_${e}_111.txt
    cp ./test_${e}_111_smallexample/log_${e}_111/expe_${e}_111_jobs.csv ./test_smallexample/expe_${e}_111_jobs.csv
    cp ./test_${e}_111_smallexample/log_${e}_111/expe_${e}_111_machine_states.csv ./test_smallexample/expe_${e}_111_machine_states.csv
    cp ./test_${e}_111_smallexample/log_${e}_111/expe_${e}_111_schedule.csv ./test_smallexample/expe_${e}_111_schedule.csv
done 

for e in 0
do
    rm -r test_${e}_0_smallexample
    rm -r test_${e}_111_smallexample
done 